/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.training_pertemuan_1;

import java.util.*;
import java.lang.Math;

/**
 *
 * @author windows-10
 */
public class Pertemuan1 {

    Scanner scan = new Scanner(System.in);
    
    public static void main(String[] args) {
        
        Pertemuan1 pert = new Pertemuan1();
        
//        pert.primeNumber();
//        pert.number2();
//        pert.number3();
//        pert.number4();
//          pert.number5();
        pert.number6();
       
        
    }
    
    public void primeNumber(){
        //1 Prime numbers less than N
        
        
        Scanner scan = new Scanner(System.in);
        
        int number = scan.nextInt();
        
        for(int i = 2; i < number; i++){
            if(i == 2)System.out.println(i);
            if(i%2 == 1){
                if(!checkPrime(i)){
                    System.out.println("not prime " + i);
                }else{
                    System.out.println(i);
                }
            }
            
        }
    }
    
    public static boolean checkPrime(int a){
        for(int i = 2; i < a; i++ ){
            if(a%i == 0){
                return false;
            }
            
        }
        return true;
    }
    
    public void number2(){
         //2. 1 5 14 30 55 91 140
         int add = 5;
         int n = 4;
         int start = 1;
         
         do{
             System.out.println(start);
             start += n;
             n += add;
             add += 2;
              
             
         }while(start <= 140);
    }
    
    public void number3(){
        //3. 8 27 125 512 1728
        int number = 2;
        int base = 2;
        int output;
        
        int next = 1;
        do{
            output = number*number*number;
            System.out.println(output);
            number += next;
            next += 1;
        }while(output <= 1728);
    }
    
    public void number4(){
        int jumlah;
        int max;
        int output = 0;
        
        System.out.print("Jumlah bebek: ");
        jumlah = scan.nextInt();
        System.out.print("Jumlah max: ");
        max = scan.nextInt();
        
        if(jumlah%max == 0){
            output = jumlah/max;
        }else{
            output = jumlah/max + 1;
        }
        
        System.out.println("Output: " + output );
        
    }
    
    
    public void number5(){
        
        int input;
        String output = " ";
        
        System.out.println("input: ");
        input = scan.nextInt();
        
        if(input%2 == 0){
            output += "a";
        }
        if(input%3 == 0){
            output += "b";
        }
        if(input%5 == 0){
            output += "c";
        }
        if(input%7 == 0){
            output += "d";
        }
        if(input%10 == 0){
            output += "e";
        }
        if(input%15 == 0){
            output += "f";
        }
        
        System.out.println("Output: " + output);
        
    }
    
    public void number6(){
        
        int start;
        int end;
        
        
        
        ArrayList<Integer> genap = new ArrayList<>();
        ArrayList<Integer> ganjil = new ArrayList<>();
        
        System.out.print("Start: ");
        start = scan.nextInt();
        System.out.print("End: ");
        end = scan.nextInt();
        
        for(int i = start+1; i < end; i++){
            if(i == 2)System.out.println(i);
            if(i%2 == 1){
                ganjil.add(i);
            }else{
                genap.add(i);
            }
            
        }
        
        System.out.println("Bilangan ganjil antara " + start + " dan " + end + " adalah ");
        for (Integer gan : ganjil) {
            System.out.print(gan + " ");
        }
        System.out.println("");
        System.out.println("Bilangan genap antara " + start + " dan " + end + " adalah ");
        for (Integer gen : genap) {
            System.out.print(gen + " ");
        }
    } 
}
